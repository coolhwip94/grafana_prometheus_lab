# Grafana Learn
 
>This repo host files required to stand up a lab instance involving `Grafana`, `Prometheus`, and `Mock metrics` (python script producing prometheus metrics)  
> This is intended to provide a brief understanding of how to consume and display metrics in Grafana.

## Setup
---
- An environment with docker is required
```
docker-compose up -d
```

## Overview
---
- Running command above will instantiate 3 docker containers
  - grafana container
  - mock_data container (python script producing data with prometheus client)
  - prometheus container


## Containers
---
> The descriptions provided here are for the default behavior, this can be adjusted in the docker-compose.yml

<br>

- ### Grafana Container
  - UI Accessible through `http:<ip or hostname>/3000`
  - Default loging is admin/admin (please do not change if in the lab)

<br>

- ### Prometheus Container
  - UI Accessible through `http:<ip or hostname>/9090`
  - Prometheus will scrape or listen for metrics on `http:<ip or hostname>/8000/metrics`
    - This is configured in `prometheus_materials/prometheus.yml`
      ```
      - job_name: 'mock_metrics'
      scrape_interval: 10s
      static_configs:
        - targets: ['10.11.1.27:8000'] 

      ```
      > This example shows my labserver ip
  - `prometheus_materials/prometheus.yml` also includes a job to poll itself for metrics

<br>

- ### Mock Metrics container
  - This container simply runs a python `FastAPI` app to ouput metrics to `http:<ip or hostname>/8000/metrics`
  - The type of metrics can be adjusted in `mock_metrics.py`



## References
---
- https://www.youtube.com/watch?v=h4Sl21AKiDg (Highly suggested for a good overview prior to lab)
- https://medium.com/aeturnuminc/configure-prometheus-and-grafana-in-dockers-ff2a2b51aa1d
- https://hub.docker.com/r/prom/prometheus/
- https://github.com/prometheus/client_python
