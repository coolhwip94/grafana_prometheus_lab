FROM python:3.8

COPY mock_metrics.py /
COPY requirements.txt /

RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install pip -y
RUN pip install -r requirements.txt

ENTRYPOINT ["python","mock_metrics.py"]
